import QtQuick 2.13
import QtQuick.Window 2.13
import QtQml 2.13
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.2

Window {

    width: 640
    height: 480
    visible: true
    title: qsTr("Hydroboard")
    id: root
    color: "black"

    property var name: "";
    property var year: 0;
    property var month: 0;
    property var date: 0;
    property var hour: 0;
    property var minute: 0;
    property var server: "http://10.100.1.42/"

    ColumnLayout {

        anchors.fill: parent
        spacing: 0
        layoutDirection: Qt.LeftToRight
        Rectangle {
            Layout.fillWidth: true
            height: 100
            color: "#303030"
            Text {
                anchors.centerIn: parent
                text: qsTr("Hydroboard")
                font.pointSize: 16
                font.bold: true
                color: "white"
            }
        }

        Rectangle {
            height: 30
            color: "#4b4b4b"
            radius: 3
            Layout.margins: 3
            Layout.minimumWidth: 0
            Layout.minimumHeight: 30
            Layout.preferredHeight: 30
            Layout.preferredWidth: -1
            Layout.fillWidth: true

            RowLayout {
                anchors.fill: parent
                layoutDirection: Qt.LeftToRight
                Text {
                    text: qsTr("Filter:")
                }

                TextInput {
                    color: "#ffffff"
                    text: qsTr("Name")
                    verticalAlignment: Text.AlignVCenter
                    cursorVisible: true
                    Layout.margins: 3
                    Layout.fillHeight: true
                    Layout.minimumWidth: 100
                    Layout.preferredWidth: 100
                    onTextChanged: {
                        if(text !== qsTr("Name")) {
                            name = text;
                            getBoard();
                        }
                    }
                }

                ComboBox {
                    id: monthBox
                    height: 30
                    Layout.preferredHeight: -1
                    Layout.minimumHeight: 0
                    Layout.margins: 3
                    Layout.fillHeight: true
                    clip: false
                    model: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]
                    currentIndex: new Date().getMonth();
                    onCurrentIndexChanged: {
                        month = currentIndex + 1;
                        getBoard();
                    }
                }

                ComboBox {
                    id: dateBox
                    height: 30
                    Layout.preferredHeight: -1
                    Layout.minimumHeight: 0
                    Layout.margins: 3
                    Layout.fillHeight: true
                    clip: false
                    model: [ "1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th", "10th",
                        "11th", "12th", "13th", "14th", "15th", "16th", "17th", "18th", "19th", "20th",
                        "21st", "22nd", "23rd", "24th", "25th", "26th", "27th", "28th", "29th", "30th",
                        "31st" ]
                    currentIndex: new Date().getDate() - 1;
                    onCurrentIndexChanged: {
                        date = currentIndex + 1;
                        getBoard();
                    }
                }

                Rectangle {
                    Layout.fillWidth: true
                }
            }
        }

        ListView {
            id: boardView
            Layout.topMargin: 0
            Layout.margins: 5
            spacing: 3
            Layout.fillWidth: true
            Layout.fillHeight: true
            model: ListModel { id: boardModel }
            delegate: Rectangle {
                id: boardDelegate
                width: parent.width
                height: 20
                color: "#202020"
                RowLayout {
                    anchors.fill: parent
                    spacing: 0
                    layoutDirection: Qt.LeftToRight
                    Text {
                        text: name
                        color: "white"
                    }
                    Rectangle {
                        Layout.fillWidth: true
                    }
                    Text {
                        text: qsTr(`${month}/${date}/${year} ${hour}:${minute}`)
                        color: "white"
                    }
                    Button {
                        height: 30
                        Layout.preferredWidth: 30
                        Layout.minimumWidth: 30
                        Layout.preferredHeight: -1
                        Layout.minimumHeight: 0
                        Layout.margins: 3
                        Layout.fillHeight: true
                        text: qsTr("X")
                        onClicked: deleteBoard(id)
                    }
                }
            }
        }

        Rectangle {
            height: 30
            color: "#4b4b4b"
            radius: 3
            Layout.margins: 3
            Layout.minimumWidth: 0
            Layout.minimumHeight: 30
            Layout.preferredHeight: 30
            Layout.preferredWidth: -1
            Layout.fillWidth: true

            RowLayout {
                anchors.fill: parent
                layoutDirection: Qt.LeftToRight
                Text {
                    text: qsTr("New record:")
                }

                TextInput {
                    id: nameInput
                    color: "#ffffff"
                    text: qsTr("Name")
                    verticalAlignment: Text.AlignVCenter
                    cursorVisible: true
                    Layout.margins: 3
                    Layout.fillHeight: true
                    Layout.minimumWidth: 100
                    Layout.preferredWidth: 100
                }

                ComboBox {
                    id: hourBox
                    height: 30
                    Layout.preferredHeight: -1
                    Layout.minimumHeight: 0
                    Layout.margins: 3
                    Layout.fillHeight: true
                    clip: false
                    model: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 ]
                    currentIndex: new Date().getHours() - 1;
                }

                Text {
                    text: ":"
                }

                ComboBox {
                    id: minuteBox
                    height: 30
                    Layout.preferredHeight: -1
                    Layout.minimumHeight: 0
                    Layout.margins: 3
                    Layout.fillHeight: true
                    clip: false
                    model: [ "00", "05", 10, 15, 20, 25, 30, 35, 40, 45, 50, 55 ]
                    currentIndex: Math.floor(new Date().getMinutes()/5);
                }

                Button {
                    height: 30
                    Layout.preferredHeight: -1
                    Layout.minimumHeight: 0
                    Layout.margins: 3
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    clip: false
                    id: button
                    text: qsTr("Post")
                    onClicked: putBoard()
                }
            }

            MessageDialog {
                id: putError
                title: qsTr("Error")
                onAccepted: visible=false;
                visible: false
            }
        }
    }

    Dialog {
        id: serverDialog
        visible: false
        title: qsTr("Input server address")
        standardButtons: StandardButton.Ok
        onAccepted: {
            server = serverInput.text;
            connectServer();
        }
        onRejected: connectServer();
        TextInput {
            anchors.fill: parent
            height: 20
            width: 300
            id: serverInput
            text: server
            Rectangle {
                id: serverInputRect
                color: "white"
                anchors.fill: parent
                border.color: "black"
                border.width: 1
                z: -1
            }
        }
    }

    Timer {
        id: timer
        interval: 10*1000
        onTriggered: getBoard();
        running: false
        repeat: false // Only runs after successful server retrievals, does not repeat, in order to avoid endless loop of attempts if connection to server dies
    }

    // Component.onCompleted: serverDialog.open();

    function connectServer() {
        var req = new XMLHttpRequest();
        req.open("GET", server + "api/get");

        req.onreadystatechange = function() {
            if(req.readyState === 4 && req.status === 200) {
                try { // valid connection will always return JSON, even if results are empty
                    var res = JSON.parse(req.responseText);
                } catch(e) {
                    serverDialog.open();
                    return;
                }
                getBoard();
            }
            if(req.status > 200 || req.status === 0) {
                serverDialog.open();
            }
        }

        req.send("year=3000"); // there shouldn't be any records at year 3000, minimizes bandwidth usage
    }

    function getBoard() {
        var req = new XMLHttpRequest();
        var url = server + "api/get?";
        var args = 0;

        if(year > 0)    { url += "year="                             + year;  args = 1; }
        if(month > 0)   { url += (args ? "&month="   : "month=")     + month; args = 1; }
        if(date > 0)    { url += (args ? "&date="    : "date=")      + date;  args = 1; }
        if(hour > 0)    { url += (args ? "&hour="    : "hour=")      + hour;  args = 1; }
        if(minute > 0)  { url += (args ? "&minute="  : "minute=")    + minute;          }

        req.open("GET", url);

        req.onreadystatechange = function() {
            if(req.readyState === 4 && req.status === 200) {
                boardModel.clear();
                var board = {};
                try {
                    board = JSON.parse(req.responseText);
                } catch (e) {
                    putError.text = "There was an error processing the server data.";
                    putError.open();
                    return;
                }

                if(name !== "" && name !== "Name") {
                    board.results = board.results.filter(
                        i => i.name.toLowerCase().includes(name.toLowerCase())
                    );
                }

                board.results = board.results.map((i) => {
                    if(i.minute < 10) i.minute = `0${i.minute}`;
                    return i;
                })

                board.results.sort((a, b) => {
                    if(a.name > b.name) return 1;
                    else if (a.name < b.name) return -1;
                    else {
                        if(a.hour > b.hour) return 1;
                        else if(a.hour < b.hour) return -1;
                        else {
                            if(a.minute > b.minute) return 1;
                            else if(a.minute < b.minute) return -1;
                            else return 0;
                        }
                    }
                });

                board.results.forEach((i) => {
                    boardModel.append(i);
                });
                timer.start(); // Only run after a successful retrieval from the server
            }
            if(req.readyState === 4 && (req.status > 200 || req.status === 0)) {
                serverDialog.open();
            }
        }

        req.send();
    }

    function putBoard() {

        if(!nameInput.text.length || nameInput.text == "Name") {
            putError.text = qsTr("You must choose a name before recording.");
            putError.visible = true;
            return;
        }

        var req = new XMLHttpRequest();
        req.open("PUT", server + "api/put");
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        req.onreadystatechange = function() {
            if(req.readyState === 4 & req.status === 200) {
                const res = JSON.parse(req.responseText);
                if(res.error) {
                    putError.text = res.error;
                    putError.visible=true;
                } else getBoard();
            }
        };

        var today = new Date();
        var data = "";

        data += `name=${nameInput.text}&`;
        data += `year=${today.getFullYear()}&`;
        data += `month=${today.getMonth()+1}&`;
        data += `date=${today.getDate()}&`;
        data += `hour=${hourBox.currentText}&`;
        data += `minute=${minuteBox.currentText}`;

        req.send(data);

    }

    function deleteBoard(id) {
        var req = new XMLHttpRequest();
        req.open("DELETE", `${server}api/delete?id=${id}`);
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        req.onreadystatechange = function() {
            if(req.readyState === 4 && req.status === 200){
                var res = {};
                try {
                    res = JSON.parse(req.responseText);
                } catch(e) {
                    putError.text = "Delete error: " + e;
                    putError.open();
                    return;
                }
                if(res.delete) {
                    getBoard();
                } else {
                    console.log(req.responseText);
                    putError.text = "There was an error deleting the requested item.";
                    if(res.error) putError.text += " " + res.error;
                    putError.open();
                }
            }
        }
        req.send();
    }
}
